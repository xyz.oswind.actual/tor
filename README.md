[![pipeline status](https://gitlab.com/xyz.oswind.actual/tor/badges/secure-control-sockets/pipeline.svg)](https://gitlab.com/xyz.oswind.actual/tor/commits/secure-control-sockets)

feature-scs
================

### Secure Control Sockets

A tor client can be hosted making privacy-as-a-service feasible. The vpn
provider nordvpn, for instance, has several torified vpn servers
available. The initial communication in this case is based on vpn
standards, but can be replaced with obfuscation of various kinds.
Unfortunately the torified vpn servers mentioned are quite variable,
from mostly-ok to frequently-poor, in performance as one would expect
without having controller access. These providers are unlikely to deploy
the massive kluge of workarounds currently required to bolt-on this
feature. Additionally, tor does not fully support this type of
multi-tenant use as that would require the controller access to be contextual.

This feature branch implements secure control sockets using rust
websockets.
