#!/usr/bin/env python

from pyvirtualdisplay import Display
from selenium import webdriver
import time

display = Display(visible=0, size=(1024, 768))
display.start()

profile = webdriver.FirefoxProfile() 
profile.set_preference("network.proxy.type", 1)
profile.set_preference("network.proxy.socks", "127.0.0.1")
profile.set_preference("network.proxy.socks_port", 1111)
profile.update_preferences() 

driver = webdriver.Firefox(firefox_profile=profile)

driver.set_window_size(1024, 768)
driver.set_script_timeout(30)
driver.set_page_load_timeout(30)
driver.get('https://check.torproject.org/')
driver.save_screenshot('check-torproject-org.png')   

driver.quit()
display.stop()